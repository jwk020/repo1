/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110waw
 *
 */
public abstract class Temperature
{
	public abstract Temperature toKelvin();
	public float value;
	public Temperature(float v)
	{
		value = v;
	
	}
	public final float getValue()
	{
		return value;
	}
	public abstract Temperature toCelsius();
	public abstract Temperature toFahrenheit();
}

