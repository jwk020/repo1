/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110waw
 *
 */
public class Kelvin extends Temperature {


	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Temperature toKelvin(){
		return null;
		
	}
	public Kelvin(float t) 
	{ 
	 super(t); 
	} 
	 
	public String toString() 
	{ 
	 return ""+ value + " K";
	}
	@Override
	public Temperature toCelsius() {
		value= ((value - 32) *(5/9));
		Celsius temp = new Celsius(value);
		return temp;
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		value = ((value*9)/5)+32;
		Fahrenheit temp = new Fahrenheit(value);
		return temp;
	}

}
